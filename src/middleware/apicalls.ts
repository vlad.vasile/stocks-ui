
// import axiosModule, { AxiosInstance } from 'axios'

// const timeout = 9000
// export const revobetAxios: AxiosInstance = axiosModule.create({
//   timeout,
// })

// revobetAxios.interceptors.request.use(
//   config => {
//     //  before request is sent
//     if (config.params == null) {
//       config.params = {}
//     }
//     Object.assign(config.params, {
//       timestamp: new Date().getTime(),
//     })

//     const debugTestError = window.localStorage.getItem('debugTestError')
//     if (debugTestError) {
//       Object.assign(config.params, {
//         debugTestError,
//       })
//     }
//     // localStorage contains only strings
//     const debugModeEnabled = window.localStorage.getItem('debugMode') === 'true'
//     if (debugModeEnabled) {
//       Object.assign(config.params, {
//         trace: true,
//       })
//     }
//     return config
//   },
//   error => {
//     return Promise.reject(error)
//   },
// )

// revobetAxios.interceptors.response.use(
//   response => {
//     const method = response?.config?.method?.toLowerCase()
//     if (method === 'get' || method === 'post') {
//       if (response?.data?.success === false) {
//         return Promise.reject(response)
//       }
//       if (method === 'get' && response.data !== '' && typeof response.data !== 'object') {
//         console.log(method + ' response has no data', response.config.url, response)
//       }
//     }

//     return response
//   },
// )
